<?php

namespace FAQ;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{    
	protected $fillable = array('answer');
	
    public function question()
    {
        return $this->belongsTo('FAQ\Question');
    }

}
