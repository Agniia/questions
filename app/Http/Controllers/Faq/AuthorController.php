<?php

namespace FAQ\Http\Controllers\Faq;

use Illuminate\Http\Request;
use FAQ\Http\Controllers\Controller;
use FAQ\Author;
use FAQ\Category;

class AuthorController extends Controller
{
       
    public function index()
    { 
        $categories = Category::all();     
        return view('faq_main')->with('categories', $categories);
    }
}
