@extends('faq_main')

@section('questions')
<section class="questions">
    <div class="row  m-b-md align-items-center justify-content-between">
            <div class="col-6"><h3>Задать вопрос по категории  "{{ $currentCategory->category }}"</h3></div>
            <div class="col-6"><a href="{{ route('faq.category', ['category_id' => $currentCategory->id] ) }}">Вопросы и ответы категории "{{ $currentCategory->category }}"</a></div>
    </div>
    <form action="{{ route('faq.store') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="category_id" value="{{ $currentCategory->id }}">
        <div class="form-group">
            <label for="email">Введите Ваш email</label>
            <input type="email" class="form-control" id="email" aria-describedby="email" placeholder="Ваш email" name="email" required>
       </div>
       <div class="form-group">
            <label for="name">Введите Ваше имя</label>
            <input type="name" class="form-control" id="name" aria-describedby="name" placeholder="Ваше имя" name="name" required>
       </div>
       <div class="form-group">
            <label for="question">Задайте Ваш вопрос</label>
            <input type="question" class="form-control" id="question" aria-describedby="question" placeholder="Ваш вопрос" name="question" required>
       </div>
       <button type="submit" class="btn btn-primary">Задать вопрос</button>    
   </form>
 </section>        
@endsection
