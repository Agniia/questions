<?php

namespace FAQ\Http\Controllers\Admin;

use Illuminate\Http\Request;
use FAQ\Http\Controllers\Controller;
use FAQ\Question;
use FAQ\Author;
use FAQ\Category;
use FAQ\Status;
use FAQ\Answer;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category_id)
    {
        $questions = Question::where('category_id',$category_id)
         ->with('author')->with('answer')->with('status')->get();
        return view('admin.questions_list')->with('questions',$questions);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($question_id)
    {
        $question = Question::where('id',$question_id)
         ->with('author')->with('answer')->with('status')->with('category')->first();
        
        $authors = Author::all();        
        $categories = Category::all();
        $statuses = Status::all();
       return view('admin.question_edit')->with('question',$question)->with('authors',$authors)->with('categories',$categories)->with('statuses',$statuses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $question = Question::find($id);
        $question->question = $request->question;
        $question->category_id = $request->category_id;
        $question->author_id = $request->author_id;
        $question->save();
        return redirect()->route('questions.edit', ['id' => $id]);
    }

}
