@extends('layouts.faq')

@section('content')
<div class="content">
    <div class="title m-b-md">
        FAQ
     </div><!-- title m-b-md -->
     <div class="links m-b-md">
        @foreach($categories as $category)
            <a href="{{ route('faq.category', ['category_id' =>  $category->id]) }}">{{ $category->category}}</a>
        @endforeach
    </div><!-- links -->     
    
     
    @yield('questions')    
  </div><!-- content --> 
@endsection
