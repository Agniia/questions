<div id="menu">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Администраторы
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="{{ route('admin.index') }}">Все администраторы</a>
          <a class="dropdown-item" href="{{ route('admin.create') }}">Добавить администратора</a>
          <a class="dropdown-item" href="{{ route('admin.updatelist') }}">Изменить пароль администратора</a>
          <a class="dropdown-item" href="{{ route('admin.destroylist') }}">Удалить администратора</a>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Категории вопросов
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="{{ route('categories.index') }}">Все категории</a>
          <a class="dropdown-item" href="{{ route('categories.create') }}">Добавить категорию</a>
          <a class="dropdown-item" href="{{ route('categories.destroylist') }}">Удалить категорию и все вопросы в ней</a>
        </div>
      </li>
    </ul>
  </div>
</nav>

</div><!-- menu -->
