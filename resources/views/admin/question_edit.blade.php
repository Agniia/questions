@extends('home')

@section('admin-content')
<div class="admin-content">
<h4>Изменить вопрос</h4>
    <form action="{{ route('questions.update', ['id_question' => $question->id ])}}" method="post">
        @method('PUT')
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="question">Вопрос</label>
            <input type="text" class="form-control" id="question" aria-describedby="question" value="{{ $question->question }}" name="question">
       </div>
      <div class="form-group">
            <label for="created_at">Дата вопроса</label>
            <input type="text" class="form-control" id="created_at" aria-describedby="created_at" placeholder="{{ $question->created_at }}" name="created_at" disabled>
       </div>
      <div class="form-group">
            <label for="forCategory">Категория вопроса</label>
            <select class="form-control" id="category_id" name="category_id">
                  <option value="{{ $question->category->id }}" selected>{{ $question->category->category }}</option>
                  @foreach($categories as $category)
                    <option value="{{ $category->id  }}" >{{ $category->category }}</option>
                  @endforeach
            </select>
       </div>
       <div class="form-group">
            <label for="forAuthor">Автор вопроса</label>
            <select class="form-control" id="author_id" name="author_id">
              <option value="{{ $question->author->id  }}" selected>{{ $question->author->name }}</option>
              @foreach($authors as $author)
                <option value="{{ $author->id  }}" >{{ $author->name }}</option>
              @endforeach
            </select>
       </div>
        <div class="form-group">
            @if($question->answer)
                <label for="answer">Ответ на вопрос</label>
                <input type="text" class="form-control" id="answer" aria-describedby="answer" placeholder="{{ $question->answer->answer }}" name="answer" disabled>
            @endif
       </div> 
       <div class="form-group">
            <label for="status">Статус вопроса - "{{ $question->status->status }}"</label>
       </div> 
       <button type="submit" class="btn btn-primary">Изменить</button>    
       @if($question->answer)
            <a href="{{ route('answers.edit', ['id' => $question->id ] ) }}" class="btn btn-primary">Изменить ответ и статус вопроса</a>  
       @else
             <a href="{{ route('answers.create', ['question_id' => $question->id ] ) }}" class="btn btn-primary">Добавить ответ и изменить статус вопроса</a>  
       @endif
   </form>
   
 </div>   
@endsection
