@extends('home')

@section('admin-content')
<div class="admin-content">
<h4>Добавить администратора</h4>

    <form action="{{ route('admin.store')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="email">Введите email администратора</label>
            <input type="email" class="form-control" id="email" aria-describedby="email" placeholder="email администратора" name="email" required>
       </div>
       <div class="form-group">
            <label for="name">Введите имя администратора (name)</label>
            <input type="text" class="form-control" id="name" aria-describedby="name" placeholder="Имя администратора (name)" name="name" required>
       </div>
       <div class="form-group">
            <label for="username">Введите имя администратора (username)</label>
            <input type="text" class="form-control" id="username" aria-describedby="username" placeholder="Имя администратора (username)" name="username" required>
       </div>
       <div class="form-group">
            <label for="password">Введите пароль администратора</label>
            <input type="password" class="form-control" id="password" aria-describedby="password" placeholder="Пароль администратора" name="password" required>
       </div>
       <button type="submit" class="btn btn-primary">Добавить администратора</button>    
   </form>
   
 </div>   
@endsection
