@extends('home')

@section('admin-content')
<div class="admin-content">
<h4>Список категорий</h4>
    <table class="table">
        <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Название</th>
              <th scope="col">Всего вопросов</th>
              <th scope="col">Опубликовано</th>
              <th scope="col">Без ответов</th>
              <th scope="col">Перейти к вопросам</th>
            </tr>
         </thead>
        <tbody>
        @foreach($allCategories as $category)
        <tr>
          <th scope="row">{{  $category->id }}</th>
          <td>{{  $category->category   }}</td>
          <td>{{ $category->questions()->count() }}</td>
          <td>{{ $category->questions()->active()->where('category_id',$category->id)->count() }}</td>
          <td>{{ $category->questions()->waiting()->where('category_id',$category->id)->count() }}</td>
          <td><a href="{{ route('questions.index', ['id_category' => $category->id ]) }}" class="btn btn-primary">Перейти</a></td>    
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
