<?php

namespace FAQ;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = array('category');
    
    public function questions()
    {
        return $this->hasMany('FAQ\Question');
    }
}
