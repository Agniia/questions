<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Faq\AuthorController@index')->name('faq');

Route::resource('faq', 'Faq\QuestionController')->only([
    'store'
]);
Route::get('/faq/questions/category/{category_id}','Faq\QuestionController@bycategory')->name('faq.category');
Route::get('/faq/questions/create/category/{category_id}','Faq\QuestionController@create')->name('faq.create');

Auth::routes();

Route::middleware(['auth'])->group(function () {
    
    Route::namespace('Admin')->group(function () {
        
        Route::resource('admin', 'AdminController')->only([
            'index','create', 'store', 'destroy','edit','update'
        ]);
        Route::prefix('admin')->group(function () {
            Route::get('/admin/updatelist/','AdminController@updatelist')->name('admin.updatelist');
            Route::get('/admin/destroylist/','AdminController@destroylist')->name('admin.destroylist');       
        });
        
        Route::resource('questions', 'QuestionController')->only([
            'index','edit', 'update'
        ]);
        Route::get('/questions/category/{category_id}','QuestionController@index')->name('questions.index');
        
        Route::resource('answers', 'AnswerController');
        Route::get('/answers/create/question/{question_id}', 'AnswerController@create')->name('answers.create');
        
        Route::resource('categories', 'CategoryController')->only([
            'index','create', 'store', 'destroy'
        ]);
        Route::get('/categories/destroylist', 'CategoryController@destroylist')->name('categories.destroylist');
    });
});








