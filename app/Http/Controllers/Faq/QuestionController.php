<?php

namespace FAQ\Http\Controllers\Faq;

use Illuminate\Http\Request;
use FAQ\Http\Controllers\Controller;
use FAQ\Question;
use FAQ\Category;
use FAQ\Author;

class QuestionController extends Controller
{
    
    public function bycategory($category_id)
    {
         $questions = Question::active()
            ->with('author')
            ->with('answer')
            ->with('status')
            ->with('category')
            ->where('category_id', $category_id)
            ->get();           
           $categories = Category::all();     
          return view('faq.questions_by_category', compact('questions','categories'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($category_id)
    {
         $currentCategory = Category::find($category_id);
         $categories = Category::all();     
     	 return view('faq.ask_question')->with('currentCategory', $currentCategory)->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $author = Author::firstOrCreate(
        array(
                'name' => $request->name, 
                'email' => $request->email
        ));
        $question = Question::create(
        array(
            'category_id' =>$request->category_id, 
            'question' => $request->question, 
            'status_id' =>1, 
            'author_id' =>$author->id
        ));
       
       $categories = Category::all();   
       $currentCategory = Category::find($request->category_id);
       return view('faq.question_add_info')->with('currentCategory', $currentCategory)->with('author', $request->name)->with('email', $request->email)->with('question', $request->question)->with('categories',$categories);
    }
}
