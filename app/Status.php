<?php

namespace FAQ;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function questions()
    {
        return $this->hasMany('FAQ\Question');
    }
}
